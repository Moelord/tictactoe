const express = require('express');
const location = require('location');
const app = express();
const ind = require('./index');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded(
    {
        extended: true
    })
);
let x, y, winNumber, obj = [], i = 0;
const port = 3000;

app.get('/createGame', function(req, res)
{
    let played = 'O';
    x = parseInt(req.query.x);
    y = parseInt(req.query.y);
    winNumber = parseInt(req.query.winNumber);

    obj[i] =
        {
            id: i,
            gameBoard: ind.createBoardInit(x, y),
            win: winNumber,
            played: 'O'
        };

    res.json({"board": obj[i].gameBoard, "id": i});

    i++;

});

app.post('/games/:id/moves', function (req, res) //Work in progress
{
    let ix = parseInt(req.body.x);
    let iy = parseInt(req.body.y);
    let id = req.params.id;

    if(obj[id].gameBoard[ix][iy] === "_")
    {
        //board = ind.insert(ix, iy, played, board);
        if(obj[id].played === 'X')
        {
            obj[id].gameBoard[ix][iy] = 'O';
            obj[id].played = 'O';
        }
        else
        {
            obj[id].gameBoard[ix][iy] = 'X';
            obj[id].played = 'X';
        }

        if (ind.win(obj[id].gameBoard, x, y, obj[id].win, obj[id].played))
        {
            res.json({"board": obj[id].gameBoard, "win": obj[id].played});
        }
        else if(ind.tie(obj[id].gameBoard, x, y))
        {
            res.json({"board": obj[id].gameBoard, "win": "tie"});
        }
        else
        {
            if (obj[id].played === 'X')
            {
                res.json({"board": obj[id].gameBoard, "playing": 'X', "id hry": obj[id].id});
            }
            else
            {
                res.json({"board": obj[id].gameBoard, "playing": 'O', "id hry": obj[id].id});
            }
        }
    }
    else
    {
        res.json({"response": "Špatný input"});
    }
});

app.listen(3000,() => {console.log('Application listening on port ' + port)});