Piskvorky v konzoli (Node.js)
    -1. Appka se spusti z konzole prikazem node index.js (+parametry)
    0. Uzivatel zada parametry (velikost pole, pocet vyhernich)
    1. Vykresli se prazdne herni pole (console.log(), console.table(), console.clear())
    napr. ___ ___ ___ ___
         |_x_|___|_o_|___|
         |___|___|___|___|
    2. System se zepta kam chce dalsi hrac jet
    3. Hrac zada souradnice x,y
    4. System zkontroluje, jestli je zadane pole volne, pokud ne - zpet na step 2
    5. Vykresli se aktualizovane herni pole s novym tahem
    6. System zkontroluje, jestli hrac vyhral, pokud ne - zpet na step 1.
    7. Zobrazi se hlaska o vitezi a moznost dat odvetu (bezvyznamne bonusove body, pokud se bude pocitat skore obou hracu do vypnuti appky)
    
    Request pro vytvoření hry:
POST: /games

{
  "width": 4,
  "height": 5
}

Response example:
{
  "id": "unikatni ID hry",
  "board": [], // pole nebo pole polí obsahující aktualní stav hry
  "playing": "x", // kdo je nařadě v tahu
  "width": 4,
  "height": 5
}

-----------------------------------
Request pro vypsání všech her:
GET: /games

Response example:
[
  {
    "id": "unikatni ID hry",
    "board": [], // pole nebo pole polí obsahující aktualní stav hry
    "playing": "x", // kdo je nařadě v tahu
    "width": 4,
    "height": 5
  }, 
  {
    "id": "jine unikatni ID hry",
    "board": [], // pole nebo pole polí obsahující aktualní stav hry
    "playing": "x", // kdo je nařadě v tahu
    "width": 4,
    "height": 5
  }, 
]

-----------------------------------
Request pro vypsání všech her:
GET: /games/unikatni-id-hry

Response example:
{
  "id": "unikatni-id-hry",
  "board": [], // pole nebo pole polí obsahující aktualní stav hry
  "playing": "x", // kdo je nařadě v tahu
  "width": 4,
  "height": 5
}

-----------------------------------
Request pro tah ve hře
POST: /games/unikatni-id-hry/moves

{
  "player": "x",
  "coordinates": {
    "x": 1,
    "y": 2
  }
}

Response example:
{
  "id": "unikatni ID hry",
  "board": [], // pole nebo pole polí obsahující aktualní stav hry
  "playing": "o", // kdo je nařadě v tahu
}


V případě chybného requestu vracejte příslušný HTTP status code (4XX) https://httpstatuses.com/ a jednotnou strukturu erroru v response body. Například:
{
  "errors": [
    {
      "message": "Špatná hodnota šířky hrací plochy.",
      "hint": "Šířka pole musí být celé kladné číslo."
    }
  ]
}