/*
    Piskvorky v konzoli (Node.js)
    -1. Appka se spusti z konzole prikazem node index.js (+parametry)
    0. Uzivatel zada parametry (velikost pole, pocet vyhernich)
    1. Vykresli se prazdne herni pole (console.log(), console.table(), console.clear())
    napr. ___ ___ ___ ___
         |_x_|___|_o_|___|
         |___|___|___|___|
    2. System se zepta kam chce dalsi hrac jet
    3. Hrac zada souradnice x,y
    4. System zkontroluje, jestli je zadane pole volne, pokud ne - zpet na step 2
    5. Vykresli se aktualizovane herni pole s novym tahem
    6. System zkontroluje, jestli hrac vyhral, pokud ne - zpet na step 1.
    7. Zobrazi se hlaska o vitezi a moznost dat odvetu (bezvyznamne bonusove body, pokud se bude pocitat skore obou hracu do vypnuti appky)
*/
const express = require('express');
const app = express();
const server = require('./server');

function createBoardInit(ix, iy)    // má za práci inicializovat playboard
{
    let board = [];
    for(let i = 0; i < iy; i++)
    {
        board[i] = [];
        for(let j = 0; j < ix; j++)
        {
            board[i][j] = "_"; //vyplnění podtržítek
        }
    }
    return board;
}


/*function drawBoard(x, y) //vymaže konzoli a vykreslí hrací pole
{
    for(let i = 0; i < y; i++)
    {
        for(let j = 0; j < x; j++)
        {
            process.stdout.write(playBoard[i][j] + " ");
        }
        process.stdout.write("\n");
    }
}*/

function insert(ix, iy, played, board)
{
    let playBoard = board;
    if(played === 'X')
    {
        playBoard[ix][iy] = 'O';
    }
    else
    {
        playBoard[ix][iy] = 'X';
    }
    return playBoard;
}

function win(playBoard, x, y, winNumber, played)      //logika výhry
{
    let winCount = 0; //winCount pro počítání znaků co jsou vedle sebe
    for(let i = 0; i < y; i++)  //vertikální logika výhry
    {
        for(let j = 0; j < x; j++)
        {
            if(playBoard[j][i] === played)
            {
                winCount++;
                if(winCount === winNumber)
                {
                    return true;
                }
            }
            else
            {
                winCount = 0;
            }
        }
        winCount = 0;
    }

    winCount = 0;

    for(let i = 0; i < y; i++)  //horizontální logika výhry
    {
        for(let j = 0; j < x; j++)
        {
            if(playBoard[j][i] === played)
            {
                winCount++;
                if(winCount === winNumber)
                {
                    return true;
                }
            }
            else
            {
                winCount = 0;
            }
        }
    }

    for(let i = 0; i <= x-winNumber; i++)   //diagonální logika výhry
    {
        for(let j = 0; j <= y-winNumber; j++)
        {
            winCount = 0;
            for(var k = 0; k < winNumber; k++)
            {
                if(playBoard[j + k][i + k] !== played)
                {
                    break;
                }
            }
            if(k === winNumber)
            {
                return true;
            }
        }
    }

    for(let i = winNumber - 1; i <= x; i++) //diagonální logika výhry 2
    {
        for(let j = 0; j <= y-winNumber; j++)
        {
            winCount = 0;
            for(var l = 0; l < winNumber; l++)
            {
                if(playBoard[j + l][i - l] !== played)
                {
                    break;
                }
            }
            if(l === winNumber)
            {
                return true;
            }
        }
    }

    return false;
}

function tie(playBoard, x, y)
{
    let underscoreCnt = 0; //nulování
    for(let i = 0; i < y; i++)  //remíza
    {
        for(let j = 0; j < x; j++)
        {
            if(playBoard[j][i] === "_")
            {
                underscoreCnt++;    //počítá se počet '_', pokud žádné nejsou a nikdo nevyhrál, tak nastává remíza
            }
        }
    }
    if(underscoreCnt === 0)
    {
        return true;
    }
    return false;
}

module.exports =
    {
        createBoardInit,
        insert,
        win,
        tie
    };

/*function game()
{
    while(replay)  //opakuje se pouze v případě že hráči zvolí možnost hrát znovu
    {
        playBoard = createBoardInit(x, y);
        replay = false;
        console.clear();
        drawBoard(x, y);
        let side1 = 0, side2 = 0;
        while(true)
        {
            if(played === 'X')
            {
                console.log("Je na tahu hráč 'O'");
            }
            else
            {
                console.log("Je na tahu hráč 'X'");
            }

            do  //cyklus, protože ošetření v případě že pozice je již zabraná
            {
                //side1 = parseInt(rl.question("Zadejte další tah (osa x) ").toString());
                //side2 = parseInt(rl.question("Zadejte další tah (osa y) ").toString());
                if(playBoard[side2][side1] !== '_')  //pokud je pozice prázdná, tak to uživatele pustí dál
                {
                    console.log("Zadejte to znovu \n")
                }
            }while(playBoard[side2][side1] !== '_');

            insert(side1, side2);   //tah hráče závislý na tom, kdo hrál poslední

            if(win())   //přičítá se výhra
            {
                if(played === 'X')
                {
                    winX++;
                }
                else
                {
                    winO++;
                }
                break;
            }
            else
            {
                if(tie)
                {
                    break;
                }
            }
        }
        if(tie)
        {
            console.log("Remíza");
        }
        console.log("Hráč 'X': " + winX);
        console.log("Hráč 'O': " + winO);
        let replay0 = rl.question("Replay? y/n ");
        if(replay0.toString() === 'y')  //restart
        {
            replay = true;
            tie = false;
        }
    }
}*/